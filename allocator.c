//
//  COMP1927 Assignment 1 - Memory Suballocator
//  allocator.c ... implementation
//
//  Created by Liam O'Connor on 18/07/12.
//  Modified by John Shepherd in August 2014
//  Copyright (c) 2012-2014 UNSW. All rights reserved.
//

#include "allocator.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define HEADER_SIZE    sizeof(struct free_list_header)  
#define MAGIC_FREE     0xDEADBEEF
#define MAGIC_ALLOC    0xBEEFDEAD

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#define TRUE     1
#define FALSE    0
#define YES 1
#define NO 0

typedef unsigned char byte;
typedef u_int32_t vlink_t;
typedef u_int32_t vsize_t;
typedef u_int32_t vaddr_t;
typedef int bool;


typedef struct free_list_header {
   u_int32_t magic;           // ought to contain MAGIC_FREE
   vsize_t size;              // # bytes in this block (including header)
   vlink_t next;              // memory[] index of next free block
   vlink_t prev;              // memory[] index of previous free block
} free_header_t;

// Global data
static byte *memory = NULL;   // pointer to start of suballocator memory
static vaddr_t free_list_ptr; // index in memory[] of first block in free list
static vsize_t memory_size;   // number of bytes malloc'd in memory[]

free_header_t * getNextHeader (free_header_t * header);
int findNextPowerOfTwo(u_int32_t number);
int isPowerOfTwo (int n);
free_header_t * partitionNewHeader (free_header_t * header);
void sal_partition (free_header_t * header, u_int32_t n);
void sal_merge(free_header_t * header);
void sal_validate (free_header_t * header);

/* 
how to do X

int index = (void* ) header - (void*) memory;

free_header_t * t = (free_list_header *) (memory + header_index + header->size);
free_list_header *t = (free_list_header *) (memory + free_list_ptr);

*/

int findNextPowerOfTwo(u_int32_t number){
   
   int count = 1;
     
   while (count < number) {
        
        count *= 2;
    
    }

    number = count;
    
    return number; 
}

void sal_init(u_int32_t size) {

  // check if the size given is not a power of 2
  if (!isPowerOfTwo(size)) {
    //look for the next closest power of 2 that is larger than input size
    size = findNextPowerOfTwo(size);
 
  } 

  ///malloc a section of memory with size given
  memory  = malloc(size);

  //set the index of the first free block in the free list to zero
  //this is the first position in the memory[] array
  free_list_ptr = 0;
  memory_size = size;

  //create a new header and set it to point to the start of the suballocator memory
  free_header_t *header = (void *) memory;

  //initialise starting stats for this new block
  header->magic = MAGIC_FREE;
  header->size = size;
  header->next = 0;
  header->prev = 0;

}

free_header_t * getNextHeader (free_header_t * header){
  
  int headerIndex = (void*) header - (void*) memory;
  
  return (free_header_t *) memory + headerIndex + header->size; 

}

vaddr_t getHeaderIndex (free_header_t * header){

  return ((void*) header - (void*) memory);

}



int isPowerOfTwo (int n) {

  return ((n & (~n + 1)) == n);

}

// a helper function which given a header, partitions it into two
// and returns the header of the first partition

free_header_t * partitionNewHeader (free_header_t * header){

  free_header_t * newHeader;
  int partitionSize = header->size/2;
  newHeader = (free_header_t*) memory + getHeaderIndex(header) + partitionSize;
  vaddr_t newHeaderIndex = getHeaderIndex(newHeader);

  // update pointers from first partition of header to the next
  // check end case first
  if (header->next == free_list_ptr){
        
      newHeader->next = free_list_ptr;

  } 
       
  newHeader->prev = getHeaderIndex(header);
      header->next = newHeaderIndex;

  //update header information of first and second partitions
  printf("header size =%d, set to MAGIC_FREE\n", header->size);
  header->size = partitionSize;
  printf("header->size = %d\n", header->size);
  newHeader->magic = MAGIC_FREE;
  newHeader->size = partitionSize;

  return (free_header_t *) (memory + getHeaderIndex(header)); // + header->size;

}

// a helper function which given a header and an input size for mallocing
// loops through and repeatedly splits the partitions until it produces a partition
// that is small enough that it is not wasting space
// and large enough it fits the header and the size of space for mallocing

void sal_partition (free_header_t * header, u_int32_t n) {
  
  while (header->size > HEADER_SIZE + n){
     
     header = partitionNewHeader(header);
     header->magic = MAGIC_FREE;
     printf("header size ==%d, set to MAGIC_FREE\n", header->size);
  }

  header->magic = MAGIC_ALLOC;
  sal_validate(header);
  printf("header index %d, set to MAGIC_ALLOC\n", getHeaderIndex(header));

  //return (free_header_t *) memory + getHeaderIndex(header) + header->size;

}


//loop through blocks of memory and if not malloc'd set to MAGIC_FREE
void sal_validate (free_header_t * header){
  printf("this function ran\n");
    while(header->next != 0){
       if (header->magic != MAGIC_FREE && header->magic != MAGIC_ALLOC){
         header->magic = MAGIC_FREE;
         printf("resetting something\n");
       }
       header = getNextHeader(header);
    }

}

void *sal_malloc(u_int32_t n) {
   
   // check if the size given is not a power of 2
   if (!isPowerOfTwo(n)) {

      //look for the next closest power of 2 that is larger than input size
      n = findNextPowerOfTwo(n);
 
   } 

   //traverse through free list, starting at the header pointed to by free_list_ptr
   //which points to the first free block in memory
   //and look for next free block of memory
   free_header_t * currentHeader = (free_header_t *) (memory + free_list_ptr);
  //store index of current header
   vaddr_t currentHeaderIndex = getHeaderIndex(currentHeader);

   int validHeader;
  // int count = 0;

   while ((currentHeader->size < HEADER_SIZE + n) && currentHeader->magic == MAGIC_ALLOC) {
        
        //currentHeaderIndex = (void*) currentHeader - (void*) memory;

        //Valid header check: check if arbitrary number in header is correct
        validHeader = ((currentHeader->magic == MAGIC_FREE) 
                        || (currentHeader->magic == MAGIC_ALLOC));
        
        if (!validHeader){
          printf("currentHeader->magic %d \n", currentHeader->magic);
          printf("currentHeader->size %d \n", currentHeader->size);
          fprintf(stderr, "Memory corruption");
          abort();
        }

        //Skip to the next header
        //count++;
        currentHeader = getNextHeader(currentHeader);   

   } 
   
   // beginning case: allocating the very first memory block
   if (currentHeaderIndex == 0) {

     sal_partition(currentHeader,n);
     sal_validate(currentHeader);
     printf("currentHeaderIndex = 0 ran\n");

   }
   // if the traversal loops around and reaches free_list_ptr again
   // this means it didn't find any regions big enough
   // function immediately returns NULL
   else if (currentHeader->next == free_list_ptr) {

      return NULL;

   }

   //if desired memory size could fit within half of the free region
   //partition it into two regions
   else if ( (currentHeader->size / 2) > HEADER_SIZE + n ) {
      printf("this ran\n");
      sal_partition(currentHeader,n);

   } 

   //otherwise use the current block of memory without partitioning
   else {
      
      //update header information
      currentHeader->magic = MAGIC_ALLOC;
      currentHeader->size = n;

      //how to update pointers for this case?
   }

   //Update free list pointer so it points to the next free region header
   //Traverse through the free list looking for the next free block in memory
   
   free_header_t * freeListPointerHeader = (free_header_t*) (memory + free_list_ptr);

   while ( freeListPointerHeader->magic != MAGIC_FREE ){

        freeListPointerHeader = getNextHeader(freeListPointerHeader);

   }

   free_list_ptr = getHeaderIndex(freeListPointerHeader);
   
   //return a pointer to the first byte after the region header for the allocated region
   //currentHeaderIndex = getHeaderIndex(currentHeader);
   return ((void*)(currentHeaderIndex + HEADER_SIZE));

}

void sal_merge(free_header_t * header){

   // get and store prev and next headers
   free_header_t * nextHeader = (free_header_t *) memory + header->next;
   free_header_t * prevHeader = (free_header_t *) memory + header->prev;
   free_header_t * headerAfterNextHeader = (free_header_t*) memory + nextHeader->next;
   //free_header_t * headerbeforePrevHeader = (free_header_t*) memory + prevHeader->prev;

   //checking initial conditions for merging...
   //check if adjacent regions are also free / non-allocated
   while (nextHeader->magic == MAGIC_FREE || prevHeader->magic == MAGIC_FREE) {

      //two cases
      //next region is free but previous region isn't
      if(nextHeader->magic == MAGIC_FREE && prevHeader->magic != MAGIC_FREE){

          //delete next header links and relinking pointers
          headerAfterNextHeader->prev = getHeaderIndex(header);
          header->next = getHeaderIndex(headerAfterNextHeader);

          //update header information of region
          header->size += HEADER_SIZE + nextHeader->size;

      } 
      //previous region is free but next region isn't
      else if (nextHeader->magic == MAGIC_FREE && prevHeader->magic != MAGIC_FREE){
          
          //delete current header and relinking pointers
          prevHeader->next = getHeaderIndex(nextHeader);
          nextHeader->prev = getHeaderIndex(prevHeader);
          //update header of information of previous region
          prevHeader->size += HEADER_SIZE + header->size;

      } 

   }

}

void sal_free(void *object) {

   // get and store object's header
   free_header_t *objectHeader = (free_header_t *) ((int)memory + (int)object - HEADER_SIZE);

   //make sure object is valid by checking its header's magic number
   int isValid = (objectHeader->magic == MAGIC_ALLOC);
   if (!isValid) {
      
      fprintf(stderr, "Attempt to free non-allocated memory");
      abort();
   
   }
   
   //traverse the free list starting at the first header in the free list
   //to insert this header back into the free list into memory order 
   free_header_t *curr = (free_header_t *) (memory + free_list_ptr);
   while (getHeaderIndex(object) > getHeaderIndex(curr)){
         getNextHeader(curr);
   }  
   
   //update pointers: inserting object after curr
   objectHeader->prev = getHeaderIndex(curr);
   objectHeader->next = curr->next;
   curr->next = getHeaderIndex(objectHeader);

   objectHeader->magic = MAGIC_FREE;

   //start merging operations which checks if adjacent regions are free and merges them
   sal_merge(objectHeader);
   
}

void sal_end(void) {
   free(memory);
   memory = NULL;
   memory_size = 0;
}

void sal_stats(void)
{
   // Optional, but useful
   printf("%s ++++++ sal_stats ++++++ %s\n", KCYN, KNRM);
   printf("Green Chunks: %sFree%s\n", KGRN, KNRM);
   printf("Yellow Chunks: %sAllocated%s\n", KYEL, KNRM);
   printf("Red Chunks: %sCorrupt (invalid MAGIC value)%s\n\n", KRED, KNRM);



    // we "use" the global variables here
    // just to keep the compiler quiet
   // memory = memory;
   // printf("%p\n", &memory);
   // printf("%p\n", &free_list_ptr);
   // printf("%p\n", &memory_size);



   // Loop through each block and make a graphical representation of the chunks.
   
   int cycle = 0;
   int current_chunk_idx = 0;
   while(current_chunk_idx != 0 || cycle == 0) {
      free_header_t *current_chunk = (free_header_t *) memory + current_chunk_idx;

      if (current_chunk->magic == MAGIC_ALLOC) {
         printf("%s", KYEL);
      }
      else if (current_chunk->magic == MAGIC_FREE) {
         printf("%s", KGRN);
      }
      else {
         printf("%s", KRED);
      }

      // printf("Chunk at %p\n", current_chunk);
      // printf("Size: %d\n", current_chunk->size);
      // printf("Next: %d\n", current_chunk->next);
      printf("|+++++++++++++++++|\n");
      printf("| Chunk at %06d |\n", current_chunk_idx);
      printf("| Next at %06d  |\n", current_chunk->next);
      printf("| Prev at %06d  |\n", current_chunk->prev);
      bool f = YES;
      int i;
      for (i = 0; i*64 < current_chunk->size; ++i)
      {
         if (f) {
            printf("|   size:%06d   |\n", current_chunk->size);
            f = NO;
         }
         else {
            printf("|                 |\n");
         }
         
      }

      current_chunk_idx = (current_chunk_idx + current_chunk->size) % memory_size;
      cycle ++;

      printf("%s", KNRM);
   }
   printf("|+++++++++++++++++|\n");

   // free_list_ptr = free_list_ptr;
   // memory_size = memory_size;
}
