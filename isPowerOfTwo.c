
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int isPowerOfTwo(int number);
int findNextPowerOfTwo(int number);
int main(){
	printf("isPowerOfTwo(16) = %d\n", isPowerOfTwo(16));
	printf("isPowerOfTwo(32) = %d\n", isPowerOfTwo(32));
	printf("isPowerOfTwo(64) = %d\n", isPowerOfTwo(64));
	printf("isPowerOfTwo(128) = %d\n", isPowerOfTwo(128));
	printf("isPowerOfTwo(256) = %d\n", isPowerOfTwo(256));

	printf("\n");
	printf("findNextPowerOfTwo(3) = %d\n", findNextPowerOfTwo(3));

	return 0;
}


int isPowerOfTwo(int number){
	 int result = 0;
	 if ((number & (~number+ 1)) == number) {
   	   result = 1;
     }
     return result;
}
//findNextPowerOfTwo(3) 

int findNextPowerOfTwo(int number){
	 
	 int count = 1;
     int shift = 0; 
     
     while (count < number) {
        count *= 2;
     }

     number = count;
    
    return number; 
}